'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('books', [
      {
        code: '001',
        nombre: 'CIEN AÑOS DE SOLEDAD',
        quantity: 1,
        url_image: 'https://www.google.com/search?q=imagen+cien+a%C3%B1os+de+soledad&sxsrf=AOaemvIl33laoepqrRuVM-R4wTEDHKoG0A:1636682403163&tbm=isch&source=iu&ictx=1&fir=0dMQj-f7VFRz8M%252CNTlE2NnGZ13OeM%252C_%253BXmg2cEV7rHmAGM%252CwBslGEWMskMTzM%252C_%253BOvsmfnPdVWA7OM%252CrbWeQ7cBvUY4sM%252C_%253Br8pSizT0_eA1VM%252Ckrjhg9l9zXFmNM%252C_%253BMvPLOM_UfXJKbM%252CiXCphkK6tq7V4M%252C_%253Bxmi0-HWxBi0ADM%252CL5b8O8FmJyU07M%252C_%253BmB641mvFIE5qQM%252C2fkun9SkKsYIBM%252C_%253BuD2Xi15kdPAEIM%252Csu1D3XqxQBLKkM%252C_%253Bw7CCJ5B_RaOVSM%252CvOgJYqYct4KlFM%252C_%253BhJ60IgDJd7BGxM%252Cya1H55Wje5hhcM%252C_%253Ba4D0ZHBDwCEuhM%252Cz6DG-zflOq0FEM%252C_%253BN9cxnSjN1-T4YM%252CGhqnI2rY7MI6IM%252C_%253BOEwchJkRKuGWOM%252CcTKK2zBxCe7DdM%252C_%253B4tp9X_c7FBN9rM%252C915T9pM0tCiCtM%252C_%253BovttHXwf5mVUnM%252C9lcaq6BwJQRoiM%252C_&vet=1&usg=AI4_-kTbbd2vZ1AqDB7K4XFLVYUjiPOHOw&sa=X&ved=2ahUKEwj5koCK3ZH0AhXbRTABHWmPCWkQ9QF6BAggEAE&biw=1920&bih=969&dpr=1#imgrc=OvsmfnPdVWA7OM',
        state: 1,
        updatedAt: new Date(),
        createdAt: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('books', null, {});
  }
};
