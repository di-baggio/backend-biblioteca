'use strict'
const express = require('express');
const borrowedControllers = require('../controllers/borrowed-book-controller');

let api = express.Router();

api.post('/borrowed', borrowedControllers.createBorrowedBook);
api.put('/borrowed/:borrowed_id', borrowedControllers.updateBorrowedBook);
api.get('/borrowed/:borrowed_id?', borrowedControllers.getBorrowedBook);
module.exports = api;