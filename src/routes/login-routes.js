'use strict'
const express = require('express');
const loginControllers = require('../controllers/login-controller');

let api = express.Router();

api.post('/login', loginControllers.login);
module.exports = api;