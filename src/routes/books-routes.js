'use strict'
const express = require('express');
const booksControllers = require('../controllers/books-controller');

let api = express.Router();

api.post('/book', booksControllers.createBook);
api.put('/book/:book_id', booksControllers.updateBook);
api.get('/book', booksControllers.getBooks);
api.get('/book/available', booksControllers.getBooksAvailable);
module.exports = api;