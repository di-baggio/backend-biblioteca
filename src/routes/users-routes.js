'use strict'
const express = require('express');
const usersControllers = require('../controllers/users-controller');

let api = express.Router();

api.post('/user', usersControllers.createUser);
api.put('/user/:user_id', usersControllers.updateUser);
api.get('/user', usersControllers.getUsers);
module.exports = api;