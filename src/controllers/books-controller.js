'use strict'
const db = require('../models');
const { Op } = require('sequelize');

async function createBook(req, res) {
    try {
        db.books.create({
            code: req.body.code,
            nombre: req.body.nombre,
            quantity: req.body.quantity,
            url_image: req.body.url_image,
            state: req.body.state,
        }).then((response) => {
            res.status(200).json({ message: 'Procesado correctamente', error: {}, response: { message: 'Libro creado con éxito', id: response.id } });
        }).catch(err => {
            let message;
            // Se verifica si el objeto "errors" ya que Sequelize devuelve la mayoria de errores en este objeto
            if (err.errors && err.errors.length > 0) {
                message = err.errors[0].message;
            } else {
                message = err.message;
            }
            res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: message }, response: {} });
        });
    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
async function updateBook(req, res) {
    try {
        const book_id = req.params.book_id;
        const book = await db.books.findOne({
            where: {
                id: book_id
            }
        });
        if (book) {

            book.nombre = req.body.nombre;
            book.quantity = req.body.quantity;
            book.url_image = req.body.url_image;
            book.state = req.body.state;

            book.save().then((response) => {
                res.status(200).json({ message: 'Procesado correctamente', error: {}, response: { message: 'Libro actualizado con éxito' } });
            }).catch(err => {
                let message;
                // Se verifica si el objeto "errors" ya que Sequelize devuelve la mayoria de errores en este objeto
                if (err.errors && err.errors.length > 0) {
                    message = err.errors[0].message;
                } else {
                    message = err.message;
                }
                res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: message }, response: {} });
            });
        } else {
            res.status(404).json({ mesage: 'Procesado correctamente', error: { status: 404, message: 'Libro no existe' }, response: {} });
        }

    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
async function getBooks(req, res) {
    try {
        let where;
        if (req.params.book_id) {
            where = {
                where: {
                    id: req.params.book_id
                }
            }
        }
        const books = await db.books.findAll(where);

        res.status(200).json({ message: "procesado correctamente", error: {}, response: books })
    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
async function getBooksAvailable(req, res) {
    try {

        const borrowed = await db.borrowed_books.findAll({
            where: {
                state: 1,
                return_date: null
            }
        });
        const bookNotAvailables = (borrowed && borrowed.length > 0) ? borrowed.map(value => value.books_id) : [];

        const books = await db.books.findAll({
            where: {
                state: 1,
                id: { [Op.notIn]: bookNotAvailables }
            }
        });

        res.status(200).json({ message: "procesado correctamente", error: {}, response: books })
    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
module.exports = {
    createBook,
    updateBook,
    getBooks,
    getBooksAvailable
}