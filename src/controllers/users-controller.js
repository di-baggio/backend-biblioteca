'use strict'
const db = require('../models');
const bcrypt = require('bcryptjs');
async function createUser(req, res) {
    try {
        db.users.create({
            identification_number: req.body.identification_number,
            name: req.body.name,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 10),
            state: req.body.state
        }).then((response) => {
            res.status(200).json({ message: 'Procesado correctamente', error: {}, response: { message: 'Usuario creado con éxito', id: response.id } });
        }).catch(err => {
            let message;
            // Se verifica si el objeto "errors" ya que Sequelize devuelve la mayoria de errores en este objeto
            if (err.errors && err.errors.length > 0) {
                message = err.errors[0].message;
            } else {
                message = err.message;
            }
            res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: message }, response: {} });
        });
    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
async function updateUser(req, res) {
    try {
        const user_id = req.params.user_id;
        const user = await db.users.findOne({
            where: {
                id: user_id
            }
        });
        if (user) {
            user.identification_number = req.body.identification_number;
            user.name = req.body.name;
            user.state = req.body.state;
            user.email = req.body.email;

            user.save().then((response) => {
                res.status(200).json({ message: 'Procesado correctamente', error: {}, response: { message: 'Usuario actualizado con éxito' } });
            }).catch(err => {
                let message;
                // Se verifica si el objeto "errors" ya que Sequelize devuelve la mayoria de errores en este objeto
                if (err.errors && err.errors.length > 0) {
                    message = err.errors[0].message;
                } else {
                    message = err.message;
                }
                res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: message }, response: {} });
            });
        } else {
            res.status(404).json({ mesage: 'Procesado correctamente', error: { status: 404, message: 'Usuario no existe' }, response: {} });
        }

    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
async function getUsers(req, res) {
    try {
        let where;
        if (req.query.user_id || req.query.state) {
            where = {
                where: {
                }
            }
            if (req.query.user_id) { where.where['id'] = req.query.user_id }
            if (req.query.state) { where.where['state'] = req.query.state }
        }
        const users = await db.users.findAll(where);

        res.status(200).json({ message: "procesado correctamente", error: {}, response: users })
    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
module.exports = {
    createUser,
    updateUser,
    getUsers
}