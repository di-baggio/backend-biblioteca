'use strict'
const db = require('../models');

async function createBorrowedBook(req, res) {
    try {
        db.borrowed_books.create({
            books_id: req.body.books_id,
            user_id: req.body.user_id,
            borrowed_date: req.body.borrowed_date,
            state: req.body.state,
        }).then((response) => {
            res.status(200).json({ message: 'Procesado correctamente', error: {}, response: { message: 'Préstamo creado con éxito', id: response.id } });
        }).catch(err => {
            let message;
            // Se verifica si el objeto "errors" ya que Sequelize devuelve la mayoria de errores en este objeto
            if (err.errors && err.errors.length > 0) {
                message = err.errors[0].message;
            } else {
                message = err.message;
            }
            res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: message }, response: {} });
        });
    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
async function updateBorrowedBook(req, res) {
    try {
        const borrowed_id = req.params.borrowed_id;
        const book = await db.borrowed_books.findOne({
            where: {
                id: borrowed_id
            }
        });
        if (book) {
            book.state = req.body.state;
            book.return_date = new Date().toISOString().split('T')[0];

            book.save().then((response) => {
                res.status(200).json({ message: 'Procesado correctamente', error: {}, response: { message: 'Prestamo actualizado con éxito' } });
            }).catch(err => {
                let message;
                // Se verifica si el objeto "errors" ya que Sequelize devuelve la mayoria de errores en este objeto
                if (err.errors && err.errors.length > 0) {
                    message = err.errors[0].message;
                } else {
                    message = err.message;
                }
                res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: message }, response: {} });
            });
        } else {
            res.status(404).json({ mesage: 'Procesado correctamente', error: { status: 404, message: 'Prestamo no existe' }, response: {} });
        }

    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
async function getBorrowedBook(req, res) {
    try {
        let where;
        if (req.params.book_id) {
            where = {
                id: req.params.book_id
            }
        }

        const books = await db.borrowed_books.findAll({
            where, include: [{
                association: 'user'
            }, {
                association: 'book'
            }]
        });

        res.status(200).json({ message: "procesado correctamente", error: {}, response: books })
    } catch (err) {
        res.status(500).json({ message: 'Error al procesar la consulta', error: { status: 500, message: err.message }, response: {} });
    }
}
module.exports = {
    createBorrowedBook,
    updateBorrowedBook,
    getBorrowedBook,
}