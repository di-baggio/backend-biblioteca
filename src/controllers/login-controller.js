'use strict'
const db = require('../models');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

async function login(req, res) {
    const user = await db.users.findAll({
        where: {
            email: req.body.email
        }
    });
    if (user && user.length > 0) {
        const valid_user = bcrypt.compareSync(req.body.password, user[0].password);
        if (valid_user) {
            const payload = {
                valid: true
            };
            const token = jwt.sign(payload, process.env.HASH_TOKEN_SECRET, {
                expiresIn: 1440
            });
            res.status(200).json(
                { message: 'Autenticación correcta', error: {}, response: { token } }
            );
        } else {
            res.status(403).json({ message: 'Datos inválidos', error: { message: 'Los datos suministrados está errados' }, response: {} })
        }
    } else {
        res.status(404).json({ message: 'Consulta procesada correctamente', error: { message: 'Usuario no existe' }, response: {} })
    }
}

module.exports = {
    login
}