'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class borrowed_books extends Model {
    static associate(models) {
      // borrowed_books belongs To
      borrowed_books.belongsTo(models.users, {
        as: 'user',
        foreignKey: 'user_id'
      });
      borrowed_books.belongsTo(models.books, {
        as: 'book',
        foreignKey: 'books_id'
      });
    }
  };

  borrowed_books.init({
    books_id: DataTypes.NUMBER,
    user_id: DataTypes.NUMBER,
    borrowed_date: DataTypes.DATE,
    return_date: DataTypes.DATE,
    state: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'borrowed_books',
  });
  return borrowed_books;
};