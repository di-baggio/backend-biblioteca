'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      // books has
      users.hasMany(models.borrowed_books, {
        foreignKey: 'user_id'
      });
    }
  };
  users.init({
    identification_number: DataTypes.NUMBER,
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    state: DataTypes.NUMBER
  }, {
    sequelize,
    modelName: 'users',
  });
  return users;
};