'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class books extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // books has
      books.hasMany(models.borrowed_books, {
        foreignKey: 'books_id'
      });
    }
  };
  books.init({
    code: DataTypes.STRING,
    nombre: DataTypes.STRING,
    quantity: DataTypes.NUMBER,
    url_image: DataTypes.STRING,
    state: DataTypes.NUMBER
  }, {
    sequelize,
    modelName: 'books',
  });
  return books;
};