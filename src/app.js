'use strict'

const express = require('express');
const path = require('path');
const app = express();
const cors = require('cors');

const booksRoutes = require('./routes/books-routes');
const usersRoutes = require('./routes/users-routes');
const borrowedBooksRoutes = require('./routes/borrowed-book-routes');

app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');

const corsOptions = {
    optionsSuccessStatus: 200
};

app.use(express.urlencoded({
    extended: false
}));

app.use(express.json());
app.use(cors(corsOptions));

// Routes
app.use('/api', booksRoutes);
app.use('/api', usersRoutes);
app.use('/api', borrowedBooksRoutes);

// MIDDLEWARES
app.use(function (req, res, next) {
    res.status(404).json({ message: "Error al procesar esta consulta", error: { status: 404, message: "Lo siento, no encuentro esta ruta" } });
    next();
});

module.exports = app;