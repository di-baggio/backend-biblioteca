const _env = require('node-env-file');
const path = require('path');
_env(path.join(__dirname, '../../env/environments.env'), { overwrite: true, raise: false });
module.exports =
{
  development: {
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASS,
    database: process.env.database_name,
    host: process.env.database_host,
    dialect: process.env.DATABASE_DIALECT,
    define: {
      freezeTableName: true
    }
  },
  test: {
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASS,
    database: process.env.database_name,
    host: process.env.database_host,
    dialect: process.env.DATABASE_DIALECT,
    define: {
      freezeTableName: true
    }
  },
  production: {
    username: process.env.database_username,
    password: process.env.database_password,
    database: process.env.database_name,
    host: process.env.database_host,
    dialect: process.env.DATABASE_DIALECT,
    define: {
      freezeTableName: true
    }
  }
}