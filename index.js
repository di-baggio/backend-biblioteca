const env = require('node-env-file');
env(__dirname + '/env/environments.env', { overwrite: true, raise: false });
const app = require('./src/app');
const port = process.env.PORT || 3000;

app.listen(port, function () {
    console.log(`Servidor corriendo http://localhost:${port}`);
});