
Version de nodejs (14.)
## Pasos para ejecutar el proyecto:

### Paso 1: instalar dependencias

Ejecute npm i en la terminal 

### Paso 2: configuración de variables
Dírigase a la ruta src/env/ y edite el archivo environments.env estas propiedades permiten la conección a la base de datos.
En la propiedad "DATABASE_DIALECT" puede colocar 'mysql'/'sqlite'/'postgres'/'mssql' según el motor de base de datos de preferencia.
Si el valor es diferente a "mssql" por favor instale manualmente el paquete de conexión correspondiente al motor de base de datos de preferencia.

### Paso 3: Generar la migracion
Cree una base de datos con el nombre que colocó en las variables de entorno.
En la terminal vaya a la carpeta src, una vez ahí ejecute el siguiente comando "npm sequelize db:migrate" y luego "npm sequelize db:seed:all"
Con esto ya tendrá configurada toda la estrutura de la base de datos
### Paso 4: Ejecución del proyecto:
En la terminal ejecute npm run start para ejecutar la API
